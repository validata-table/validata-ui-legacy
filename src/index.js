import { render, Form } from "goodtables-ui/src/index.js"
import Qs from "qs"
import React from "react"

import "./assets/main.scss"


const apiUrl = GOODTABLES_API_URL // Defined by Webpack config.

const schemas = [
    {
        code: "scdl-adresses",
        name: "Adresses locales",
        shortDescription: "Liste des adresses locales d'une collectivité",
        version: "v1.1",
        url: "https://git.opendatafrance.net/scdl/adresses/raw/v1.1/schema-scdl-adresses.json",
        specUrl: "http://www.opendatafrance.net/SCDL_Adresses_Locales"
    },
    {
        code: "scdl-deliberations",
        name: "Délibérations",
        shortDescription: "Liste des délibérations adoptées par une assemblée locale",
        version: "v2.0",
        url: "https://git.opendatafrance.net/scdl/deliberations/raw/v2.0/schema.json",
        specUrl: "http://www.opendatafrance.net/SCDL_Deliberations"
    },
    {
        code: "scdl-equipements",
        todo: true,
        name: "Équipements",
        shortDescription: "Liste des équipements publics gérés par une collectivité",
        specUrl: "http://www.opendatafrance.net/SCDL_Equipements_Publics"
    },
    {
        code: "scdl-marches",
        name: "Marchés publics",
        shortDescription: "Liste des marchés publics attribués par une collectivité",
        version: "v1.0.1",
        url: "https://git.opendatafrance.net/scdl/marches-publics/raw/v1.0.1/schema.json",
        specUrl: "http://www.opendatafrance.net/SCDL_Marches_Publics"
    },
    {
        code: "scdl-prenoms",
        name: "Prénoms des nouveaux-nés",
        shortDescription: "Liste des prénoms des nouveaux-nés déclarés à l’état-civil",
        version: "v1.1.1",
        url: "https://raw.githubusercontent.com/Jailbreak-Paris/liste-prenoms-nouveaux-nes/v1.1.2/prenom-schema.json",
        specUrl: "https://docs.google.com/document/d/1Vk0kpBw3MIocai9JqovLK2HxcUA_3QHnZicqxuOpcQ8/edit?usp=sharing"
    },
    {
        code: "scdl-subventions",
        name: "Subventions",
        shortDescription: "Liste des subventions publiques attribuées par une collectivité",
        version: "v1.1",
        url: "https://git.opendatafrance.net/scdl/subventions/raw/v1.1/schema.json",
        specUrl: "http://www.opendatafrance.net/SCDL_Subventions"
    },
]

const examples = [
    {
        name: "Adresses fictives invalides",
        url: "https://git.opendatafrance.net/scdl/adresses/raw/v1.1/exemples/exemple_invalide.csv",
        schemaCode: "scdl-adresses",
    },
    {
        name: "Adresses de Bayonne avril 2018",
        url: "https://git.opendatafrance.net/scdl/adresses/raw/v1.1/exemples/20180424_bal_216401026.csv",
        schemaCode: "scdl-adresses",
    },
    {
        name: "Délibérations fictives valides",
        url: "https://git.opendatafrance.net/scdl/deliberations/raw/v2.0/examples/Deliberations_ok.csv",
        schemaCode: "scdl-deliberations",
    },
    {
        name: "Délibérations fictives invalides",
        url: "https://git.opendatafrance.net/scdl/deliberations/raw/v2.0/examples/DeliberationsCindoc.csv",
        schemaCode: "scdl-deliberations",
    },
    {
        name: "Marchés publics fictifs valides",
        url: "https://git.opendatafrance.net/scdl/marches-publics/raw/master/exemples/exemple_marche_public.csv",
        schemaCode: "scdl-marches",
    },
    {
        name: "Marchés publics fictifs invalides",
        url: "https://git.opendatafrance.net/scdl/marches-publics/raw/master/exemples/exemple_marche_public_avec_erreurs.csv",
        schemaCode: "scdl-marches",
    },
    {
        name: "Prénoms des nouveaux-nés Digne-les-Bains 2017",
        url: "https://raw.githubusercontent.com/CharlesNepote/liste-prenoms-nouveaux-nes/v1.1.1/DIGNE-PRENOMS-2017.csv",
        schemaCode: "scdl-prenoms",
    },
    {
        name: "Prénoms des nouveaux-nés fictifs invalides",
        url: "https://raw.githubusercontent.com/CharlesNepote/liste-prenoms-nouveaux-nes/v1.1.1/prenoms-nouveaux-nes.exemple.invalide.1.1.csv",
        schemaCode: "scdl-prenoms",
    },
    {
        name: "Subventions fictives invalides",
        url: "https://git.opendatafrance.net/scdl/subventions/raw/v1.1/exemples/exemple_invalide.csv",
        schemaCode: "scdl-subventions",
    },
    // {
    //     name: "Subventions Nièvre 2016",
    //     url: "https://git.opendatafrance.net/scdl/subventions/raw/v1.1/exemples/DEP_Ni%C3%A8vre_CD58_SUBVENTIONS_2016_LISTE.csv",
    //     schemaCode: "scdl-subventions",
    // },
    {
        name: "Erreur de format (HTML au lieu de CSV)",
        url: "https://git.opendatafrance.net/",
        schemaCode: "scdl-subventions",
    },
]

function parseJSON(response) {
    return response.json()
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        var error = new Error(response.statusText)
        error.response = response
        throw error
    }
}

function fetchJSON(url, options = {}) {
    return fetch(url, options)
        .then(checkStatus)
        .then(parseJSON)
}

function fetchJSONWithCORSProxy(url) {
    return fetchJSON(`https://cors-anywhere.herokuapp.com/${url}`)
}

function validate(source, options) {
    if (source instanceof File) {
        const formData = new FormData()
        formData.append("source", source)
        formData.append("schema", options.schema)
        return Promise.all([
            fetchJSON(apiUrl, {
                method: 'POST',
                body: formData,
            }),
            fetchJSONWithCORSProxy(options.schema),
        ]).then(values => {
            updateLocation({ schema: options.schema })
            return values
        })
    }
    const queryData = Object.assign({ source }, options)
    // TODO Check that options.schema is an URL
    return Promise.all([
        fetchJSON(`${apiUrl}?${Qs.stringify(queryData)}`),
        fetchJSONWithCORSProxy(options.schema),
    ]).then(values => {
        // Update location only when source is an URL, because it's impossible to store an uploaded File in the URL.
        updateLocation(queryData)
        return values
    })
}

function updateLocation(queryData) {
    const queryString = `${location.pathname}?${Qs.stringify(queryData)}`
    history.pushState(null, null, queryString);
}

const options = Qs.parse(location.search.slice(1))

const source = options.source
delete options.source

const element = document.getElementById('app')

const formProps = { source, options, validate, schemas, examples }
if (source) {
    // TODO Check that options.schema is an URL
    formProps.reportPromise = Promise.all([
        fetchJSON(`${apiUrl}?${Qs.stringify({ source, schema: options.schema })}`),
        fetchJSONWithCORSProxy(options.schema),
    ])
}

render(Form, formProps, element)
