# Validata

- Public instance of the application: https://dev.validata.fr/
- [Wiki](https://git.opendatafrance.net/validata/validata-ui/wikis/home)

## Project

This is the main repository for the [Validata project](http://www.opendatafrance.net/outil-de-qualification-des-donnees-ouvertes-qualidata/) (FR). The goal of Validata is to give users feedback on the _validity_ of datasets they produce in order to help them increase their quality.

_Validity_ as understood in this project means:
- Absence of general errors in file structure or content,
- Conformity to a data schema (e.g. within the [French Socle commun des données locales](http://opendatalocale.net/scdl/)).

In this project we intend to rely on well-known communities and existing projects based on state of the art technologies. In particular, we share [Frictionless Data's vision](https://frictionlessdata.io/specs/) and choose to use some of its technical building blocks (i.e. [Good Tables](http://goodtables.io/), [Data Package](https://frictionlessdata.io/data-packages/), etc.).

Here are some of the principles we adhere to:
- Manage data in Git repositories with native versioning,
- Rely on continuous integration to validate or transform data,
- Add metadata to describe the datasets and their schema.

## Dependencies

- [Elm](http://elm-lang.org/) 0.18
- [NodeJS](https://nodejs.org/en/) LTS version (v8.9.4 at the time this line is written)
- [npm](https://www.npmjs.com/)

We recommend using [nvm](https://github.com/creationix/nvm) to install NodeJS and npm:

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
nvm install --lts
nvm use --lts
```

Install the project dependencies:

```sh
npm install
```

## Development

Start the web server:

```sh
npm start
```

Open http://localhost:8080

## Deploy in production

```sh
npm run build && scp -r dist/* validata@dev.validata.fr:validata-ui-dist
```